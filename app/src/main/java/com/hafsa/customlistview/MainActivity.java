package com.hafsa.customlistview;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText editName;
    private EditText editEmail;
    private ListView listView;
    private ArrayList<User> users = new ArrayList<>();
    private UserAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editName = findViewById(R.id.name);
        editEmail = findViewById(R.id.email);
        listView = findViewById(R.id.listView);

        adapter = new UserAdapter(this, users);
        listView.setAdapter(adapter);
    }

    public void submit(View view) {
        String name = editName.getText().toString();
        String email = editEmail.getText().toString();
        users.add(new User(name, email));

        adapter.notifyDataSetChanged();
        Toast.makeText(this, "Total Items: " + users.size(), Toast.LENGTH_SHORT).show();
    }

    public void clearList(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

        alert.setTitle("Clearing list");
        alert.setMessage("Are you sure?");

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                users.clear();
                adapter.notifyDataSetChanged();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "You didn't agree.", Toast.LENGTH_SHORT).show();
            }
        });
        alert.setCancelable(false);
        alert.show();
    }

    public void openGoogle(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("url", "http://www.prothomalo.com");
        startActivity(intent);
    }
}
