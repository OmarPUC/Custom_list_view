package com.hafsa.customlistview;

public class User {

    private String name;
    private String email;

    public User(String name, String emial) {
        this.name = name;
        this.email = emial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmial() {
        return email;
    }

    public void setEmial(String emial) {
        this.email = emial;
    }
}
